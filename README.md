# La Grande Aventure #

## Résumé ##
**La Grande Aventure** raconte l'histoire de notre héros voyageant de villes en villes, de régions en régions pour rencontrer l'aventure. Au cours de son périple, il rencontrera de nombreux personnages l'aidant (ou non) dans son périeux périple.

![aventure](http://img11.hostingpics.net/pics/64318430d5ffdb7bdessinetonaventure18568.jpg)

Suivez avec nous cette incroyable aventure ! En apprenant à utiliser **GIT** vous pourrez nous aider à continuer cette merveilleuse histoire. 

## Clone ##
Vous pouvez directement cloner ce répertoire sur votre ordinateur via la commande 

`git clone https://PSEUDO@bitbucket.org/Derwaan/la-grande-aventure.git`

en remplaçant *PSEUDO* par votre identifiant BitBucket.

## Participer à l'aventure ##
Si vous souhaitez continuer l'aventure avec nous, vous pouvez librement **forker** ce répertoire et proposer des **pulls requests**. 

### Règles ###
- Veuillez respectez le travail des autres. Ne supprimez pas toute une partie pour la simple et bonne raison que celle-ci ne vous plait pas. Essayez de faire avancer l'histoire.
- Restez correct. Les propos racistes, violents ou à tendances sexuelles sont interdits.
- Ne renvoyez pas de requêtes identiques alors que la première a déjà été refusée.
- Ne nous harcelez pas pour voir votre requête acceptée. Nous nous occupons de chaque cas un par un selon notre temps disponible.

Toutes requêtes ne respectant pas ces règles ne pourra être acceptées. Nous nous gardons le droit de refuser toute requête.

### Branches ###
- Si vous êtes étudiant à l'UCL, vous devez effectuer vos requêtes vers la branche *ucl*
- Si vous êtes étudiant de l'ECAM, vous devez effectuer vos requêtes vers la branche *ecam*
- Si vous ne faites pas partie d'une des catégories listées ci-dessus, vous devez effectuer vos requêtes vers la branche *other*

**Aucune requête envoyée sur la branche master ne pourra être acceptée !**

Vous pouvez si vous le souhaitez rajouter votre nom et votre adresse mail dans le fichier *CONTRIBUTORS*.
Votre participation est la bienvenue !

## License ##
[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
